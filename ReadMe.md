# epoll_demo


* Build : 

```bash
$ yum install -y gcc

$ make
```


* Usage : 

```bash
$ ./server
server <listen port>.

$ ./client
client <server ip> <server port> <client number> <connect speed>.

```


* Demo : 

```bash
# Shell 1
$ ulimit -n 65536
$ ./server 12345

# Shell 2
$ ulimit -n 65536
$ ./client 127.0.0.1 12345 20000 1000

# Shell 3
# View Current Connects
$ watch -n1 ss -s
```


